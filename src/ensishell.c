/*****************************************************
 * Copyright Grégory Mounié 2008-2015                *
 *           Simon Nieuviarts 2002-2009              *
 * This code is distributed under the GLPv3 licence. *
 * Ce code est distribué sous la licence GPLv3+.     *
 *****************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "variante.h"
#include "readcmd.h"

#include "sys/wait.h"
#include <fcntl.h>

#ifndef VARIANTE
#error "Variante non défini !!"
#endif

/* Guile (1.8 and 2.0) is auto-detected by cmake */
/* To disable Scheme interpreter (Guile support), comment the
 * following lines.  You may also have to comment related pkg-config
 * lines in CMakeLists.txt.
 */

#if USE_GUILE == 1
#include <libguile.h>

int question6_executer(char *line)
{
    /* Question 6: Insert your code to execute the command line
     * identically to the standard execution scheme:
     * parsecmd, then fork+execvp, for a single command.
     * pipe and i/o redirection are not required.
     */
    printf("Not implemented yet: can not execute %s\n", line);

    /* Remove this line when using parsecmd as it will free it */
    free(line);

    return 0;
}

SCM executer_wrapper(SCM x)
{
    return scm_from_int(question6_executer(scm_to_locale_stringn(x, 0)));
}
#endif


void terminate(char *line) {
#if USE_GNU_READLINE == 1
    /* rl_clear_history() does not exist yet in centOS 6 */
    clear_history();
#endif
    if (line)
        free(line);
    printf("exit\n");
    exit(0);
}

struct pid_cell {
    pid_t pid;
    char **cmd;
    struct pid_cell *next;
};

int main() {
    printf("Variante %d: %s\n", VARIANTE, VARIANTE_STRING);

#if USE_GUILE == 1
    scm_init_guile();
    /* register "executer" function in scheme */
    scm_c_define_gsubr("executer", 1, 0, 0, executer_wrapper);
#endif

    // Creation de la liste des fils du shell
    struct pid_cell *childs = NULL;

    while (1) {
        struct cmdline *l;
        char *line=0;
        int i;
        char *prompt = "ensishell>";

        /* Readline use some internal memory structure that
           can not be cleaned at the end of the program. Thus
           one memory leak per command seems unavoidable yet */
        line = readline(prompt);
        if (line == 0 || ! strncmp(line,"exit", 4)) {
            terminate(line);
        }

#if USE_GNU_READLINE == 1
        add_history(line);
#endif


#if USE_GUILE == 1
        /* The line is a scheme command */
        if (line[0] == '(') {
            char catchligne[strlen(line) + 256];
            sprintf(catchligne, "(catch #t (lambda () %s) (lambda (key . parameters) (display \"mauvaise expression/bug en scheme\n\")))", line);
            scm_eval_string(scm_from_locale_string(catchligne));
            free(line);
            continue;
        }
#endif

        /* parsecmd free line and set it up to 0 */
        l = parsecmd( & line);

        /* If input stream closed, normal termination */
        if (!l) {

            terminate(0);
        }

        if (l->err) {
            /* Syntax error, read another command */
            printf("error: %s\n", l->err);
            continue;
        }

        if (l->in) printf("in: %s\n", l->in);
        if (l->out) printf("out: %s\n", l->out);

        // Initialise les pipes
        int nb_pipes=0;
        while (l->seq[nb_pipes]!=0)
            nb_pipes++;
        int fds[--nb_pipes][2];
        for (i=0; i<nb_pipes; i++)
            pipe(fds[i]);

        // Conserver les pids
        int pids[nb_pipes+1];

        // Parcours les commandes
        for (i=0; l->seq[i]!=0; i++)
        {
            // Recupere la i-ieme ommande
            char **cmd = l->seq[i];
            // Met à jours la liste des fils non termines
            struct pid_cell *cur_cell = childs;
            struct pid_cell *prev_cell;
            while (cur_cell != NULL)
            {
                int status;
                if (waitpid(cur_cell->pid, &status, WNOHANG)<0)
                {
                    if (cur_cell == childs) childs = childs->next;
                    else prev_cell->next = cur_cell->next;
                    free(cur_cell);
                    cur_cell = prev_cell;
                }
                prev_cell = cur_cell;
                cur_cell = cur_cell->next;
            }
            // Execute la i-eme commande
            pid_t pid;
            switch (pid = fork())
            {
                // Echec du fork
                case -1:
                    printf("*** ERROR: Forking child process failed\n");
                    exit(1);
                    break;
                    // Fils
                case 0:
                    // Redirection de l'entree standard
                    if (i == 0 && l->in)
                    {
                        // Ouvre le fichier en lecture
                        int fdin;
                        if ((fdin = open(l->in, O_RDONLY)) < 0) {
                            printf("*** ERROR: Couldn't open input file\n");
                            exit(1);
                        }           
                        // Duplique fdin sur l'entree standard
                        dup2(fdin, 0);
                        // Ferme le descripteur
                        close(fdin);
                    }
                    // Redirection de la sortie standard
                    if (i == nb_pipes && l->out)
                    {
                        // Ouvre le fichier en ecriture
                        int fdout;
                        if ((fdout = open(l->out, O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)) < 0) {
                            printf("*** ERROR: Couldn't open output file\n");
                            exit(1);
                        }           
                        // Duplique fdout sur la sortie standard
                        dup2(fdout, 1);
                        // Ferme le descripteur
                        close(fdout);
                    }
                    // Si pas 1ere cmd
                    if (i>0) {
                        close(fds[i-1][1]);
                        //
                        dup2(fds[i-1][0], 0);
                    }
                    // Si pas derniere cmd
                    if (i < nb_pipes) {
                        // Ferme la sortie du ieme tube
                        close(fds[i][0]);
                        // Dup SDTOUT avec l'entree du ieme tube
                        dup2(fds[i][1], 1);    
                    }

                    if (strcmp(cmd[0], "jobs") == 0)
                        // Custom "jobs" cmd
                    {
                        // Affiche les jobs
                        struct pid_cell *cur_cell = childs;
                        while (cur_cell != NULL)
                        {
                            printf("%ld: ", (long)cur_cell->pid);
                            for (int index=0; cur_cell->cmd[index]!=0; index++)
                            {
                                printf("%s ", cur_cell->cmd[index]);
                            }
                            printf("\n");
                            cur_cell = cur_cell->next;
                        }
                        // Fin de commande
                        exit(0);
                    } 
                    else if (execvp(cmd[0], cmd) < 0)
                        // Execution failed
                    {
                        printf("*** ERROR: execvp failed\n");
                        exit(1);
                    }
                    break;
                    // Pere
                default:
                    // Enregistre le pid
                    pids[i] = pid;

                    // Ferme le tube prec en ecriture
                    if (i > 0) {
                        close(fds[i-1][1]);
                    }

                    if (l->bg)                    
                    {                
                        // Processus en background, on l'ajoute a la liste des fils
                        struct pid_cell *new = malloc(sizeof(struct pid_cell));
                        new->pid = pid;
                        int len_cmd;
                        for (len_cmd=0; cmd[len_cmd]!=0; len_cmd++);
                        len_cmd++;
                        char **cmd_cpy = malloc(len_cmd*sizeof(char *));
                        for (int k=0; cmd[k]!=0; k++)
                        {
                            int len = 0;
                            for (int l=0; cmd[k][l]!=0; l++) len++;
                            len++;
                            cmd_cpy[k] = malloc(len*sizeof(char));
                            memcpy(cmd_cpy[k], cmd[k], len);
                        }
                        cmd_cpy[len_cmd-1]=0;
                        new->cmd = cmd_cpy;
                        new->next = childs;
                        childs = new;
                    }
                    break;
            }
        }
        // Si pas de background, attend l'execution du fils
        if (!l->bg) {
            int terminated_childs = 0;
            while (terminated_childs < nb_pipes+1)
            {
                pid_t pid = waitpid(pids[terminated_childs], NULL, 0);

                if (pid != pids[terminated_childs])
                {
                    printf("%d", pids[terminated_childs]);
                    printf("%d", pid);
                    printf("Critical error with child terminate order.");
                    exit(1);
                }

                terminated_childs++;
            }
        }
    }
}
